import { Component, NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ChatroomComponent } from "./chatroom/chatroom.component";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { ProfileComponent } from "./profile/profile.component";
import { SignUpComponent } from "./sign-up/sign-up.component";



  const routes: Routes = [
      {path:"login", component: LoginComponent},
      {path: "home", component : HomeComponent},
      {path: '', redirectTo:"home", pathMatch:"full"},
      {path: 'signUp', component: SignUpComponent},
      {path : 'chat', component: ChatroomComponent},
      {path:"profile",component : ProfileComponent}
      
  ];



   @NgModule({
       imports: [RouterModule.forRoot(routes)],
       exports: [RouterModule]
   })
   export class AppRoutingModule {}