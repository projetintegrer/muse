export class User {
    id : number;
    firstName: string;
    lastName : string;
    addresse : string;
    country : string;
    age : number;
    gender : string; 
    email : string;
    password : string;

}