import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../model/User';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  p : User = new User()


  constructor(private us : UserService, private _router: Router) { }

  ngOnInit() {
  }


  addUser(){
    this.us.addUser(this.p).subscribe(()=>this._router.navigateByUrl("/login"));
  }
}
