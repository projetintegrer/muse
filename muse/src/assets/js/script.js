$(function () {

    /* Smooth scroll */

    $("[data-scroll]").on("click", function (event) {
        event.preventDefault();

        var blockId = $(this).data('scroll'),
            blockOffset = $(blockId).offset().top;

        $("html, body").animate({
            scrollTop: blockOffset
        }, 800);
    });

    /* Top Button */

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#topBtn').fadeIn();
        } else {
            $('#topBtn').fadeOut();
        }
    });

    $('#topBtn').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
    });

});

$(function () {

    /* Nav burger */
    $("#nav_toggle").on("click", function (event) {
        event.preventDefault();
        $(this).toggleClass("active");
        $("#nav").toggleClass("active");
        $("#static").toggleClass("static");
    });
});

// LOADER

jQuery(window).on('load', function () {
    'use strict';
    jQuery('#loader').css('opacity', 0);
    setTimeout(function () {
        jQuery('#loader').hide();
    }, 350);
});